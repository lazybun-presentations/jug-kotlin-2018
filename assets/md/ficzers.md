## Features

+++

## Type inference

+++

```java
public static void main(String[] args) {
  User user = new User();

  user.setUsername("JUG");
  user.setPassword("hunter2");

  final String username = user.getUsername();
}
```

Note:
Here we have simple Java code which initializes variable user, sets username and password data to it and then stores username into a finalized variable.

+++

```kotlin
fun main(args: Array<String>) {
  var user = User(
      username = "JUG",
      password = "hunter2"
  )
  
  val username = user.username
}
```

@[2](Regular **var**iable)
@[7](Final **val**ue)

Note:
Here's the same code in Kotlin - difference being the type inference. For your disposal, there are two keywords:
* var - which is a regular variable
* val - which is a finalized variable

+++

```java
public static void main(String[] args) {
  var user = new User();

  user.setUsername("JUG");
  user.setPassword("hunter2");

  final var username = user.getUsername();
}
```

@[2,7](Java10)

Note:
Of course you could say: Bah, why should I switch to Koltin when Java10 has type inference as well? Well, Java10's type inference is a local type inference, unlike Kotlin's which can be use wherever.

+++

## Data classes

+++

```java
public class User {

  private String username;

  private String password;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    User user = (User) o;
    return Objects.equals(username, user.username) &&
        Objects.equals(password, user.password);
  }

  @Override public int hashCode() {

    return Objects.hash(username, password);
  }

  @Override public String toString() {
    return "User{" +
        "username='" + username + '\'' +
        ", password='" + password + '\'' +
        '}';
  }
}
```

@[3-5](Fields)
@[6-21](Getters & Setters)
@[23-33](Equals)
@[35-38](Hash)
@[40-45](toString)

Note:
As you very well know, creating data class in java is cumbersome. You need to: (list stuff). Sure, your IDE can generate it for you, but still - this is a lot of code.

+++

```kotlin
data class User(
    var username: String,
    var password: String
)
```

@[1-4](Fields, Getters, Setters, Equals, Hash, toString and final)

Note:
In Kotlin, all of this stuff is neatly wrapped in data class. 

+++

```java
@Data
public class User {

  private String username;

  private String password;

}   
```

@[1](Lombok)

Note:
You could of course rebut, saying: Bah, why should I use Kotlin when I could just add Lombok to Java and be done with it? And you would be right - Lombok can be a very nice tool when used right. But still, it feels great - having it included in base language.

+++

## Null safety

Note:
One of killer features

+++

```java
User user = null;

if (user != null) {
  user.doStuff();
}
```

Note:
You're most likely all too familiar with null checking in Java. If you forget it - it could lead to fatal NullPointerException. 

+++

```kotlin
val user: User? = null

user?.doStuff() 
```
@[1](Nullable value)
@[3](Do stuff if user is not null)

Note:
In Kotlin, there null are safe. When declaring a variable, you decide whether it's nullable or not. It's very simple - just add ? to type definition!

+++

```kotlin
val user: User = null //Compilation Error
```
@[1](Non nullable value)

Note:
It's not just for show of course - if you try to assign null to a non nullable variable, then compilation error will be thrown.

+++

## Smart casts

+++

```java
Object o = new User();

if (o instanceof User) {
  User user = (User) o;
  
  user.setUsername("JUG");
  user.setPassword("hunter2");
}
```

@[3](Check type)
@[4](Cast)
@[6-7](Use)

+++

```kotlin
var o: Any = User()

if (o is User) {
  o.username = "JUG"
  o.password = "hunter2"
}
```

@[3](Check type)
@[4-5](Smart cast!)

+++

```kotlin
val user: User? = User()

user?.doSomething()

if (user != null) {
  user.doSomething() //user: User? -> User
}
```

@[3](Nullable :()
@[5](Check whether user is null)
@[6](User smartcast to non nullable!)

+++

## Extension functions

Note:
I have to say it's my personal favourite.

+++

```java
SomeLegacyClassNobodyWantsToTouch foo;
```

+++

```java
public class SomeLegacyClassNobodyWantsToTouchUtils {
  
  public static void ourOperation(
    SomeLegacyClassNobodyWantsToTouch foo, 
    String smth
  ) {
    foo.sendSomething(smth);
    foo.doSomething();
    //...
  }

}
```

+++

```kotlin
fun SomeLegacyClassNobodyWantsToTouch.ourOperation(
  smth: String
) {
  this.sendSomething(smth)
  this.doSomething()
  //...
}
```

@[1](Extension function)

+++

```java
SomeLegacyClassNobodyWantsToTouch foo 
  = new SomeLegacyClassNobodyWantsToTouch();

ourOperation(foo, "JUG");
```

+++

```kotlin
val foo 
  = SomeLegacyClassNobodyWantsToTouch()
foo.ourOperation("JUG")
```

+++

```kotlin
fun Service.doSomethingOr(
  data: String, 
  onException: (Exception) -> Unit = {}
) {
  try {
    this.doActions(data)
  } catch (e: Exception) {
    onException(e)
  }
}
```

@[5-6](Try to do actions)
@[7-8](But on exception run `onException` lambda)

+++

```kotlin
val service = Service()

service.doSomethingOr("JUG") {
  log.warn("Do something failed", it)
}
```

@[3-5](Nice and readable)

+++

## String interpolation

+++

```java
public String toString() {
    return "User{username='" + username +
     "', password='" + password + "'}";
  }
```

+++

```kotlin
fun toString(): String {
  return "User{username='$username', password='$password'}"
}
```

+++

## And many many more

* delegates
* infix functions
* unchecked exceptions
* inline functions
* properties
* operator overloading
* coroutines

Note:
There are, of course, more features that are fanmtastic. Due to limited time, I've just listed few that are my personal favourites. I encourage you to visit Kotlin's page and see Java to Kotlin comparision