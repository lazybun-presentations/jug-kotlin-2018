## But what are the interoperability caveats?

Note:
Ok, so features are fine and all, but what about using it with our beloved Java?

---

## Java -> Kotlin

+++

![Could be better](assets/image/great.gif)

+++

## !

```kotlin
val user = getUserFromJava() // type == User!
```

@[1](I just always assume user is nullable)

---

## Kotlin -> Java

+++

![Could be better](assets/image/could-be-better.gif)

+++

## Static

```java
public class View {
    public static final int VISIBLE = 0x00000000;
    public static final int INVISIBLE = 0x00000004;
    public static View inflate(
      Context context,
      int resource
    ) {
        //...
    }
}
```

+++

```kotlin
class View {
    companion object {
        @JvmField 
        val VISIBLE: Int = 0x00000000
        @JvmField 
        val INVISIBLE: Int = 0x00000004
        @JvmStatic
        fun inflate(context: Context, resource: Int) {...}
    }
}
```
@[3, 5, 7](Annotations :()
+++

```java
View.VISIBLE;

View.Companion.getVISIBLE();
```

@[1](With annotations)
@[3](Without annotations)

+++

## Classes closed by default

+++

![Spring](assets/image/spring.png)

+++

Solved by compiler plugin provided by JetBrains

Note:
Opens classes annotated with `@Component` etc